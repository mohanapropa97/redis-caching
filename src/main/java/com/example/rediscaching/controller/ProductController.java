package com.example.rediscaching.controller;


import com.example.rediscaching.dto.ProductDto;
import com.example.rediscaching.service.ProductService;
import lombok.Data;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@Data
@RestController
@RequestMapping("/product")

public class ProductController {

    private final ProductService productService;

    @PostMapping("/save")
    public ResponseEntity<String> createProduct(@RequestBody ProductDto productDto) {
        String message = productService.saveProduct(productDto);
        return new ResponseEntity<>(message, HttpStatus.CREATED);
    }

}
