package com.example.rediscaching.service;


import com.example.rediscaching.dto.ProductDto;
import com.example.rediscaching.entity.Product;
import com.example.rediscaching.repository.ProductRepository;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
@Data
@Service
public class ProductService {
    private final ProductRepository productRepository;

    public String saveProduct(ProductDto productDto) {
        if (productRepository.existsByProductID(productDto.getProductID()))
            return "Product already Exist with this EmployeeID";
        else {
            Product product = dtoToEntity(productDto);
            productRepository.save(product);
            return "Saved Successfully";
        }
    }
    private Product dtoToEntity(ProductDto productDto){
        Product product= new Product();
        BeanUtils.copyProperties(productDto,product);
        return product;
    }

}
