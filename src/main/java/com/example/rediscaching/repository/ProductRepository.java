package com.example.rediscaching.repository;


import com.example.rediscaching.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long> {

    Boolean existsByProductID(long productID);

}
